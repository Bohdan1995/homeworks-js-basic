//1 
let paragraphs = document.getElementsByTagName("p");
for(let p of paragraphs){
    p.style.backgroundColor = '#ff0000';
}


//2

let id = document.getElementById('optionsList');
console.log(id);
console.log(id.parentNode);
console.log(id.childNodes);

//3
let elem = document.querySelectorAll('#testParagraph');
console.log(elem); 

for(let elements of elem){
  elements.textContent = 'This is a paragraph'
}

//4
let item = document.querySelector('.main-header');
let list = item.childNodes;
console.log(list);
let ele = item.children
console.log(ele);
for ( let listElement of ele ){
  listElement.classList.add('nav-item');
  console.log(listElement); 
}


 
//6

let sections = document.querySelectorAll('.section-title');
for(let section of sections){
  section.classList.remove('section-title')
} 
console.log(sections);